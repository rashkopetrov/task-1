# Frontend

### System requirements

* Using the app on a bare-metal machine
    * Node.js: >=12
    * npm: >=8
    * yarn >= 1.22.19

### Setup:

Copy the `.env.dist` file to `.env` and set the environment variables:
* REACT_APP_API_URL
* REACT_APP_GOOGLE_MAPS_API_KEY

The `REACT_APP_API_URL` should point to the Backend application. If the application is run with the provided docker-compose file, then the URL should be `http://localhost:8080/api/map/geocode`.

To obtain the `REACT_APP_GOOGLE_MAPS_API_KEY`, please following the instructions on the [Google Maps Platform](https://developers.google.com/maps/documentation/javascript/get-api-key)


### Commands:


**Running the application**

`yarn && yarn start`

Once the command is executed, the application will be available at `http://localhost:3000/`

### Dev Notes

The UI consists of a text field, google map, and open street map.

The flow is:
* user types a location into the search field
* user clicks on the search button
* the onSearch action is triggered
  * two separate requests are made to the BE API with following parameters
    * provider: \[google or openstreet\]
    * address: the value from the input
  * both maps, the Google Maps and the Open Street Map, get updated

Room for improvements:
* improve the UI styling
* add error handling if a request fails
* remove the hardcoded values (zoom index and map dimensions) from the map components

----

# Backend

### System requirements

* Using the app on a bare-metal machine
    * PHP: >=8.1
    * Composer dependency manager for PHP
* Using virtualization
    * docker
    * docker-compose

### Setup

Copy the `.env.dist` file to `.env` and set the environment variables:
* REACT_APP_GOOGLE_MAPS_API_KEY

To obtain the `REACT_APP_GOOGLE_MAPS_API_KEY`, please following the instructions on the [Google Maps Platform](https://developers.google.com/maps/documentation/javascript/get-api-key)

### Endpoints

* geocode
    * request
        * GET /api/map/geocode
    * parameters
        * provider
            * required
            * string
            * values: google|openstreet
        * address
            * required
            * string
    * example:
        * curl --location --request GET 'http://localhost:8080/api/map/geocode?provider=google&address=NewYork'
        * curl --location --request GET 'http://localhost:8080/api/map/geocode?provider=openstreet&address=NewYork'


### Commands

**Installing the composer dependencies**

```bash
composer install
```

**Running the unit tests only**

```bash
composer run phpunit
```

**Check if the coding standards are met**

```bash
composer run test-cs
```

**Automatically fix the coding standards**

```bash
composer run fix-cs
```

**Running the phpunit and the coding standards test at the same time**

```bash
composer run test
```

### Using Docker & Docker Compose

**Running the application**

`docker-compose up -d`

**SSH into the docker container**

`docker-compose exec application bash`

Once you run this command, you will be inside the container's shell and can run any command that you need.

To exit the container's shell, simply type exit.

**Stopping the application**

`docker-compose down`

### Dev Notes

A minimal `leafphp` MVC boilerplate has been used to achieve the task.

Room for improvements:
* better validations and error handling
* maybe using a factory class to dynamically determine the map service in the map controller. Currently, a "match" is used.
* more unit and integration tests
