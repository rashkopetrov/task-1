import React, { useState } from 'react';

const SearchForm = (props) => {
  const [inputText, setInputText] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    props.onSearch(inputText);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        {props.label}
        <input
          type="text"
          value={inputText}
          onChange={(e) => setInputText(e.target.value)}
        />
      </label>
      <button type="submit">{props.buttonText}</button>
    </form>
  );
};

export default SearchForm;
