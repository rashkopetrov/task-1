import React, { useEffect } from 'react';
import { MapContainer, TileLayer, Marker, useMap } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

const containerStyle = {
  width: '100%',
  height: '400px',
};

const zoom = 10;

const center = {
  lat: 0,
  lng: 0,
};

const Map = ({ coordinates }) => {
  const RecenterAutomatically = (props) => {
    const map = useMap();
    const targetCoordinates = props.position || center;
    useEffect(() => {
      map.setView(targetCoordinates, zoom);
    }, targetCoordinates);
    return null;
  };

  const icon = new L.Icon({
    iconUrl: 'https://leafletjs.com/examples/custom-icons/leaf-red.png',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
  });

  return (
    <MapContainer
      style={containerStyle}
      center={coordinates || center}
      zoom={zoom}
    >
      <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      {coordinates ? (
        <Marker position={coordinates} icon={icon}></Marker>
      ) : null}
      <RecenterAutomatically position={coordinates}></RecenterAutomatically>
    </MapContainer>
  );
};

export default Map;
