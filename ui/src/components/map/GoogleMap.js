import React from 'react';
import { GoogleMap, Marker, LoadScript } from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '400px',
};

const zoom = 10;

const center = {
  lat: 0,
  lng: 0,
};

const Map = ({ coordinates }) => {
  return (
    <LoadScript googleMapsApiKey={process.env.REACT_APP_GOOGLE_MAPS_API_KEY}>
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={coordinates || center}
        zoom={zoom}
      >
        {coordinates ? <Marker position={coordinates} /> : null}
      </GoogleMap>
    </LoadScript>
  );
};

export default Map;
