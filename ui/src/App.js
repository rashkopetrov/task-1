import React, { useState } from 'react';
import SearchForm from './components/form/SearchForm';
import GoogleMap from './components/map/GoogleMap';
import OpenStreetMap from './components/map/OpenStreetMap';

const App = () => {
  const [googleMapsCoordinates, setGoogleMapsCoordinates] = useState(null);
  const [openStreetMapsCoordinates, setOpenStreetMapsCoordinates] =
    useState(null);

  const updateGoogleMapsLocation = async (address) => {
    const response = await fetch(
      `${
        process.env.REACT_APP_API_URL
      }?provider=google&address=${encodeURIComponent(address)}`
    );
    const { lat, lng } = await response.json();
    setGoogleMapsCoordinates({ lat, lng });
  };

  const updateOpenStreetMapsLocation = async (address) => {
    const response = await fetch(
      `${
        process.env.REACT_APP_API_URL
      }?provider=openstreet&address=${encodeURIComponent(address)}`
    );
    const { lat, lng } = await response.json();
    setOpenStreetMapsCoordinates({ lat, lng });
  };

  const handleSearch = async (address) => {
    await updateGoogleMapsLocation(address);
    await updateOpenStreetMapsLocation(address);
  };

  return (
    <div>
      <SearchForm
        label="Enter address:"
        buttonText="Show on map"
        onSearch={handleSearch}
      />
      <OpenStreetMap coordinates={openStreetMapsCoordinates} />
      <GoogleMap coordinates={googleMapsCoordinates} />
    </div>
  );
};

export default App;
