<?php

require __DIR__ . '/vendor/autoload.php';

Dotenv\Dotenv::createUnsafeImmutable(__DIR__)->load();

app()->cors([
    'origin' => '*',
    'methods' => 'GET',
    'allowedHeaders' => '*',
    'exposedHeaders' => '',
    'credentials' => false,
    'maxAge' => null,
    'preflightContinue' => false,
    'optionsSuccessStatus' => 204,
]);

require __DIR__ . '/routes/api.php';
require __DIR__ . '/routes/web.php';

app()->run();
