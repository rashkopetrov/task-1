<?php
app()->setNamespace("\App\Controller");

app()->mount('/api/map', static function () {
    app()->get('/geocode', 'MapGeocodeController@index');
});

