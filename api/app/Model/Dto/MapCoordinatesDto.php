<?php

declare(strict_types=1);

namespace App\Model\Dto;

final class MapCoordinatesDto
{
    public function __construct(
        public readonly float $lat,
        public readonly float $lng
    ) {
    }

    public function getLat(): float
    {
        return $this->lat;
    }

    public function getLng(): float
    {
        return $this->lng;
    }
}
