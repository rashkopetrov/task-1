<?php

declare(strict_types=1);

namespace App\Service;

use App\Mapper\MapCoordinatesMapper;
use App\Model\Dto\MapCoordinatesDto;
use App\Service\Interface\MapServiceInterface;
use Geocoder\Exception\Exception;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Query\GeocodeQuery;
use Psr\Http\Client\ClientInterface;

final class GoogleMapsService extends MapService implements MapServiceInterface
{
    private GoogleMaps $provider;

    private MapCoordinatesMapper $dtoMapper;

    public function __construct(?ClientInterface $httpClient = null)
    {
        parent::__construct($httpClient);

        $googleMapApiKey = getenv('GOOGLE_MAP_API_KEY') ?: '';
        $this->provider = new GoogleMaps($this->httpClient, null, $googleMapApiKey);

        $this->dtoMapper = new MapCoordinatesMapper();
    }

    /**
     * @throws Exception
     */
    public function findCoordinates(string $address): MapCoordinatesDto
    {
        $addressResults = $this->provider->geocodeQuery(
            GeocodeQuery::create($address)
        );
        $coordinates = $addressResults->first()->getCoordinates();

        return $this->dtoMapper->map([
            'lat' => $coordinates->getLatitude(),
            'lng' => $coordinates->getLongitude(),
        ]);
    }
}
