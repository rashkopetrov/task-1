<?php

declare(strict_types=1);

namespace App\Service;

use GuzzleHttp\Client as HttpClient;
use Psr\Http\Client\ClientInterface;

class MapService extends Service
{
    protected const USER_AGENT = 'Application/1.0';

    protected ClientInterface $httpClient;

    public function __construct(?ClientInterface $httpClient = null)
    {
        parent::__construct();

        $this->httpClient = $httpClient ?? new HttpClient();
    }
}
