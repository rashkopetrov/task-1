<?php

declare(strict_types=1);

namespace App\Service;

use App\Mapper\MapCoordinatesMapper;
use App\Model\Dto\MapCoordinatesDto;
use App\Service\Interface\MapServiceInterface;
use Geocoder\Exception\Exception;
use Geocoder\Provider\Nominatim\Nominatim;
use Geocoder\Query\GeocodeQuery;
use Psr\Http\Client\ClientInterface;

final class OpenStreetMapService extends MapService implements MapServiceInterface
{
    private const ROOT_URL = 'https://nominatim.openstreetmap.org';

    private Nominatim $provider;

    private MapCoordinatesMapper $dtoMapper;

    public function __construct(?ClientInterface $httpClient = null)
    {
        parent::__construct($httpClient);

        $this->provider = new Nominatim($this->httpClient, self::ROOT_URL, self::USER_AGENT);
        $this->dtoMapper = new MapCoordinatesMapper();
    }

    /**
     * @throws Exception
     */
    public function findCoordinates(string $address): MapCoordinatesDto
    {
        $addressResults = $this->provider->geocodeQuery(
            GeocodeQuery::create($address)
        );
        $coordinates = $addressResults->first()->getCoordinates();

        return $this->dtoMapper->map([
            'lat' => $coordinates->getLatitude(),
            'lng' => $coordinates->getLongitude(),
        ]);
    }
}
