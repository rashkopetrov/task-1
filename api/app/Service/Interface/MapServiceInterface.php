<?php

declare(strict_types=1);

namespace App\Service\Interface;

use App\Model\Dto\MapCoordinatesDto;

interface MapServiceInterface
{
    public function findCoordinates(string $address): MapCoordinatesDto;
}
