<?php

declare(strict_types=1);

namespace App\Controller;

use Leaf\Http\Request;
use Leaf\Http\Response;

class Controller
{
    public Request $request;
    public Response $response;

    public function __construct()
    {
        $this->request = new Request();
        $this->response = new Response();
    }
}
