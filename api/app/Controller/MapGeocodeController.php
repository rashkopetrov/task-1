<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\GoogleMapsService;
use App\Service\OpenStreetMapService;
use App\ServiceFinder\MapServiceFinder;

final class MapGeocodeController extends Controller
{
    public function index(): void
    {
        // a simple input validation
        $address = request()->get('address');
        $provider = request()->get('provider');
        if (!$address || !$provider) {
            response()->json([
                // at this point
                // the front end task doesn't handle server errors
                // so we always return coordinates in order to not break it
                'lat' => 0,
                'lng' => 0,

                'status' => 'error',
                'message' => 'Validation failed',
                'errors' => 'Missing address or map provider!'
            ], 422);
            exit;
        }

        $service = match ($provider) {
            'google' => new GoogleMapsService(),
            'openstreet' => new OpenStreetMapService(),
        };
        $mapServiceFinder = new MapServiceFinder($service);

        response()->json(
            $mapServiceFinder->findCoordinates($address)
        );
    }
}
