<?php

declare(strict_types=1);

namespace App\ServiceFinder;

use App\Model\Dto\MapCoordinatesDto;
use App\Service\Interface\MapServiceInterface;

final class MapServiceFinder
{
    private MapServiceInterface $mapService;

    public function __construct(MapServiceInterface $mapService)
    {
        $this->mapService = $mapService;
    }

    public function findCoordinates(string $address): MapCoordinatesDto
    {
        return $this->mapService->findCoordinates($address);
    }
}
