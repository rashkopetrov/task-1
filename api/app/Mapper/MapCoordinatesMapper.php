<?php

declare(strict_types=1);

namespace App\Mapper;

use App\Model\Dto\MapCoordinatesDto;

final class MapCoordinatesMapper
{
    public function map(array $data): MapCoordinatesDto
    {
        return new MapCoordinatesDto(
            $data['lat'] ?? 0,
            $data['lng'] ?? 0
        );
    }
}
