<?php
namespace App\Tests\Service;

use App\Service\OpenStreetMapService;
use PHPUnit\Framework\TestCase;
use Geocoder\Exception\Exception;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Client\ClientInterface;

class OpenStreetMapServiceTest extends TestCase
{
    /**
     * @throws Exception
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testFindCoordinates()
    {
        $mockHttpClient = $this->createMock(ClientInterface::class);
        $mockHttpClient->method('sendRequest')->willReturn(new Response(
            200,
            [],
            '[{"lat":42.6917571,"lon":-23.3531823}]'
        ));

        $googleMapsService = new OpenStreetMapService($mockHttpClient);
        $coordinates = $googleMapsService->findCoordinates('Sofia, Bulgaria, Serdika Center');

        $this->assertEquals([
            'lat' => 42.6917571,
            'lng' => -23.3531823
        ], [
            'lat' => $coordinates->getLat(),
            'lng' => $coordinates->getLng()
        ]);
    }
}
